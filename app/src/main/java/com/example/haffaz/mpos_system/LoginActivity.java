package com.example.haffaz.mpos_system;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.haffaz.mpos_system.sql.ConnectionClass;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class LoginActivity extends AppCompatActivity {

    ConnectionClass connectionClass;

    //UI components
    EditText etxt_id, etxt_pw;
    Button btn_login;
    ProgressBar pbbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        connectionClass =new ConnectionClass();
        etxt_id = (EditText)findViewById(R.id.et_username);
        etxt_pw = (EditText)findViewById(R.id.et_password);
        btn_login = (Button)findViewById(R.id.btn_login);
        pbbar = (ProgressBar) findViewById(R.id.pbbar);
        pbbar.setVisibility(View.GONE);

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Login loginTask = new Login();
                loginTask.execute("");
            }
        });
    }

    public void move(View view) {
        startActivity(new Intent(this, MainActivity.class));

    }

    public class Login extends AsyncTask<String,String,String> {
        String requestText = "";
        Boolean isSuccess = false;

        String userId = etxt_id.getText().toString();
        String password = etxt_pw.getText().toString();

        @Override
        protected void onPreExecute() {
            pbbar.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String r) {
            pbbar.setVisibility(View.GONE);
            Toast.makeText(LoginActivity.this,r,Toast.LENGTH_SHORT).show();

            if(isSuccess) {
                Toast.makeText(LoginActivity.this,r,Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        protected String doInBackground(String... params) {
            if(userId.trim().equals("")|| password.trim().equals("")) {
                requestText = "Please enter User Id and Password";
            }
            else {
                try {
                    Connection con = connectionClass.connect();

                    if (con == null) {
                        requestText = "Error in connection with SQL server";
                    } else {
                        String query = "select * from user where id='" + userId + "' and password='" + password + "'";
                        Statement stmt = con.createStatement();
                        ResultSet rs = stmt.executeQuery(query);

                        if(rs.next()) {
                            requestText = "Login Successful";
                            isSuccess=true;
                        }
                        else {
                            requestText = "Invalid Credentials";
                            isSuccess = false;
                        }
                    }
                }
                catch (Exception ex) {
                    isSuccess = false;
                    requestText = "Exceptions";
                }
            }
            return requestText;
        }
    }
}
