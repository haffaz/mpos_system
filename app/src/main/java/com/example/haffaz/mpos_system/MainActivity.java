package com.example.haffaz.mpos_system;

import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.pt.printer.Printer;
import android.pt.scan.Scan;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.haffaz.mpos_system.model.Item;
import com.example.haffaz.mpos_system.model.MainAdapter;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    String id;

    //UI COMPONENTS
    Button btn_scan;
    Button btn_print;

    //TextView txt_temp;

    //scanner instance
    Scan scanner = null;
    Printer printer = null;

    //Item array list instance
    List<Item> itemList;
    //Recycler view instance
    RecyclerView listView;

    boolean isOpen = false;
    //JSONObject object = new JSONObject();
    //JsonObjectRequest


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //initializing list
        itemList = new ArrayList<>();

        //calling populateList()
        //populateList();

        //initializing recycler view
        listView = findViewById(R.id.item_recycler_view);
        RecyclerView.LayoutManager lManager;
        final RecyclerView.Adapter adapter;
        lManager = new LinearLayoutManager(this);
        adapter = new MainAdapter(itemList);
        listView.setLayoutManager(lManager);
        listView.setAdapter(adapter);

        //display results temporarily
        //txt_temp = (TextView)findViewById(R.id.txt_temp);

        //scanner option
        btn_scan = (Button) findViewById(R.id.btn_search);
        btn_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scan();
            }
        });

        btn_print = (Button) findViewById(R.id.btn_print);
        btn_print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                print();
            }
        });


    }


    private void populateList(){
        //populate the list here

        //sample item data
        Item item1 = new Item("001","Something",2,500.00);
        itemList.add(item1);

    }

    //scanner
    public boolean open() {
        scanner = new Scan();
        int ret = scanner.open();

        if (ret < 0) {

            Messagebox(this, "open fail");
            isOpen = false;
        } else {
            isOpen = true;
        }
        return isOpen;
    }

    public void scan() {
        id = null;

        if (open() == false) {
            Messagebox(this, "scanner failed to open");
        } else {

            //////////////scanner
            //txt_temp.setText("");
            id = scanner.scan(5000);  //if 5 second not scanner anything,stop scanner and display "not scanner any info!!!"

            if (id == null) {
                //text.setText("not scanner any info!!!");
                Messagebox(this, "Nothing to scanner");
            } else {

                //TODO send string to database
                Messagebox(this, "Id: "+ id);
                ConnectionTask dbConnect = new ConnectionTask();
                dbConnect.execute();

            }
        }
    }

    public void close(View view) {
        if (isOpen == true) {
            int ret = scanner.close();
            if (ret < 0) {
                Messagebox(this, "close fail");
                return;
            } else {
                Messagebox(this, "close success");
            }
            scanner = null;
        } else {
            Messagebox(this, "is closed");
        }
        isOpen = false;

    }
    //scanner end


    //print
    public boolean openPrinter() {
         printer = new Printer();
        int ret = printer.open();

        if (ret < 0) {

            Messagebox(this, "open fail");
            isOpen = false;
        } else {
            isOpen = true;
        }
        return isOpen;
    }

    public void print() {
        if (openPrinter() == false) {
            Messagebox(this, "printer failed to open");

        }
        else {
            int no_paper_flg = printer.queState();

            if (no_paper_flg == 1)
            {
                Messagebox(this, "no_paper") ;
                return;
            }
            else {
                if (id != null) {
                    printer.printString("id: " +id);
                }

            }
        }
    }

    //print end



    public class ConnectionTask extends AsyncTask<String, String, String> {

        String result = null;


        @Override
        protected String doInBackground(String... strings) {
            try{
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost("http://lmuhe1zk-site.1tempurl.com/getproduct.php?id="+id);
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                InputStream is = response.getEntity().getContent();
                Log.e("DB_CONNECT", "connection success");

                //InputStream is = response.getEntity().getContent();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = bufferedReader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                this.result = sb.toString();
                Log.d("Message", "connection: " + result);
                //Toast.makeText(get, message, Toast.LENGTH_LONG).show();

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }
    }


    public static void Messagebox(Context context, String info) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("title");
        builder.setMessage(info);
        builder.setPositiveButton("yes", null);
        builder.show();
    }

    /*public class ScanTask extends AsyncTask<String,String,String> {
        boolean isOpen = false;


        *//**
     * opens scanner
     * closed, if open_flag < 0
     *
     * @return : open_flag
     *//*
        public boolean open() {
            scanner = new Scan();
            int ret = scanner.open();

            if (ret < 0) {
                Messagebox(getApplicationContext(), "open fail");
                isOpen = false;
            } else {
                isOpen = true;
            }
            return isOpen;
        }

        @Override
        protected String doInBackground(String... strings) {
            //TextView txt_temp = (TextView)findViewById(R.id.txt_temp);

            if (open() == false) {
                Messagebox(getApplicationContext(), "scanner failed to open");
            } else {

                //////////////scanner
                //txt_temp.setText("");
                String string = scanner.scanner(5000);  //if 5 second not scanner anything,stop scanner and display "not scanner any info!!!"

                if (string == null) {
                    //text.setText("not scanner any info!!!");
                    Messagebox(getApplicationContext(), "Nothing to scanner");
                } else {
                    //Messagebox(this, "Id: "+ string);
                    //TODO send string to database


                    //txt_temp.setText("id: " + string);
                }
            }
            return null;
        }
    }

    public static void Messagebox(Context context, String info) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("title");
        builder.setMessage(info);
        builder.setPositiveButton("yes", null);
        builder.show();
    }*/
}
