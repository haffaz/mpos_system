package com.example.haffaz.mpos_system.model;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.example.haffaz.mpos_system.R;

import java.util.List;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolderList>{

    List<Item> itemList; //Items List

    //Adapter Constructor
    public MainAdapter(List<Item> itemList) {
        this.itemList = itemList;
    }

    @Override
    public ViewHolderList onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.bill_item, parent,false);
        return new MainAdapter.ViewHolderList(view);
    }

    @Override
    public void onBindViewHolder(ViewHolderList holder, int position) {//updating the list UI

        holder.itemId.setText(itemList.get(position).getId());
        holder.itemName.setText(itemList.get(position).getItemName());
        holder.itemQuantity.setText(itemList.get(position).getItemQuantity());
        holder.itemPrice.setText(""+itemList.get(position).getItemPrice());

    }

    @Override
    public int getItemCount() {//Setting the list size
        return itemList.size();
    }

    public class ViewHolderList extends RecyclerView.ViewHolder {
        TextView itemId, itemName, itemQuantity, itemPrice;
        public ViewHolderList(View itemView) { //Initializing the list UI
            super(itemView);

            itemId = itemView.findViewById(R.id.item_id);
            itemName = itemView.findViewById(R.id.item_name);
            itemQuantity = itemView.findViewById(R.id.item_amount);
            itemPrice = itemView.findViewById(R.id.item_price);
        }
    }
}
