package com.example.haffaz.mpos_system.sql;

import android.annotation.SuppressLint;
import android.os.StrictMode;
import android.util.Log;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionClass {
    //String ip = "192.168.1.4";
    //String driver = "net.sourceforge.jtds.jdbc.Driver";
    //String db = "mpos_mock_db";
    String driver = "com.mysql.jdbc.Driver";
    String url = "jdbc:mysql://192.168.1.4:330/mpos_mock_db";
    String un = "root";
    String password = "";

    @SuppressLint("NewApi")
    public Connection connect() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Connection con = null;
        String ConnURL = null;
        try {

            Class.forName(driver);

            //jdbc:jtds:sqlserver://<yourDBServerIPAddress>\SQLEXPRESS:1433;databaseName=AdventureWorks;user=sa;password=*****;
            //ConnURL = "jdbc:jtds:sqlserver://" + ip + "'\'SQLEXPRESS:1433; databaseName=" + db + ";user=" + un +";password=" + password +";";

            /*ConnURL = "jdbc:jtds:sqlserver://" + ip + ";"
                    + "databaseName=" + db + ";user=" + un + ";password="
                    + password + ";";
            con = DriverManager.getConnection(ConnURL);*/

            con = DriverManager.getConnection(url, un, password);

        } catch (SQLException se) {
            Log.e("ERROR", se.getMessage());
        } catch (ClassNotFoundException e) {
            Log.e("ERROR", e.getMessage());
        } catch (Exception e) {
            Log.e("ERROR", e.getMessage());
        }
        return con;
    }
}
